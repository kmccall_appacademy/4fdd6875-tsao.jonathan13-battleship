require_relative "battleship"
require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player, :player_view

  def initialize(player = ComputerPlayer.new, board = Board.new)
    @player = player
    @board = board
    @past_moves = []
    @player_view = Board.new
  end

  def attack(position) #takes array
    @board.grid[position[0]][position[1]] = :x
    @player_view.grid[position[0]][position[1]] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    mark = @player.get_play
    while @past_moves.include?(mark)
      puts "Sorry, you've done that move already! Choose another one."
      mark = @player.get_play
    end
    @past_moves << mark
    attack(mark)
  end

  def display_status
    @player_view.display.each { |row| p row }
  end

  def play
    won = false
    @board.populate_grid
    until won
      play_turn
      display_status
      won = game_over?
      puts "You have #{count} ships left!" if !won
    end
    puts "Game over!"
  end

end
