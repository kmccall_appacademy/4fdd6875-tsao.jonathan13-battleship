class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    puts "Please enter a move in the format of (x, y) between 0 and 9."
    choice = gets.chomp.split("")
    [choice.first.to_i, choice.last.to_i]
  end


end

class ComputerPlayer
attr_accessor :options

  def initialize(name="CPU")
    @name = name
    options_create
  end


  def options_create
    @options = []
    for idx1 in 0..9
      for idx2 in 0..9
        @options << [idx1,idx2]
      end
    end
    @options.shuffle!
  end


  def get_play
    @options.pop
  end






end
