class Board

attr_accessor :grid

  def self.default_grid #factory method
    grid = []
    10.times {grid << []}
    for i in 0..9
      10.times {grid[i] << nil}
    end
    grid
  end

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def empty?(position="not given")
    if position != "not given"
      return true if @grid[position[0]][position[1]].nil?
      return false
    else
      @grid.each do |row|
        return false if row.include?(:s)
      end
      true
    end
  end

  def count
    count = 0
    @grid.each do |row|
      count += row.count(:s)
    end
    count
  end

  def full?
    @grid.each do |row|
      return false if row.include?(nil)
    end
    true
  end

  def place_random_ship
    raise "Board is full! Can't place a ship anymore." if full?
    random_pos = [rand(0...@grid.length), rand(0...@grid.length)]
    @grid[random_pos[0]][random_pos[1]] = :s if empty?(random_pos)
  end

  def won?
    return true if count == 0
    false
  end

  def [](idx)
    @grid[idx[0]][idx[1]]
  end

  def display
    @grid
  end

  def populate_grid
    10.times {place_random_ship}
    nil
  end


end
